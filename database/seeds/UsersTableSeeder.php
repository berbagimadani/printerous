<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@example.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$QCkKNZ0wV2lwHWro/loKHe.y9Vifh5.Cmo51SHI62Udf7nsTcCHRG',
                'role_id' => 1,
                'remember_token' => '4pf5xKRpZNu2KfgAA4WP02ulVhZAjNsHWBnOTtWhgt86Rzmnu2lHW9QwSizW',
                'created_at' => '2019-03-05 06:35:23',
                'updated_at' => '2019-03-05 06:35:23',
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Account Manager 1',
                'email' => 'am1@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$OJIctVP5gW8qmq5TDWEQ.uam0r7mRae1t4.xYxNhwKmQPMamguRga',
                'role_id' => 2,
                'remember_token' => 'OnKKRgKPzybeEsw9FX4eCYdeY5gf4BIibd5oElncP4HURYmJGH2UUVBcA8UE',
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            ),
            2 => 
            array (
                'id' => 3,
                'name' => 'Account Manager 2',
                'email' => 'am2@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$B9B9EtdRj6ilbdHZy5evh.zMzqzYKCkybbRXcwVXqXnJbTdlEVIgm',
                'role_id' => 2,
                'remember_token' => 'MOTbv2tX253sutNngxWB3omtgyQEnNFBONdRYcgNKfx6icVbSshl05zbADhh',
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            ),
            3 => 
            array (
                'id' => 4,
                'name' => 'Account Manager 3',
                'email' => 'am3@mail.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$vmf/Q726FNjkQ8YXbHDIpe1eqi1vi1hafh/8bAdaJWybyvBt.56I6',
                'role_id' => 2,
                'remember_token' => NULL,
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            ),
        ));
        
        
    }
}