<?php

use Illuminate\Database\Seeder;

class OrganizationPeoplesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('organization_peoples')->delete();
        
        \DB::table('organization_peoples')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2019-04-14 12:22:19',
                'updated_at' => '2019-04-14 12:22:19',
                'name' => 'pic ab makasar',
                'phone' => '0948599545',
                'email' => 'lorem@mail.com',
                'avatar' => '1555219339.png',
                'organization_id' => 2,
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2019-04-14 12:22:42',
                'updated_at' => '2019-04-14 12:22:42',
                'name' => 'pic ac makasar',
                'phone' => '34353',
                'email' => 'picabmakasar@mail.com',
                'avatar' => '1555219362.png',
                'organization_id' => 2,
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}