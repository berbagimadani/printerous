<?php

use Illuminate\Database\Seeder;

class OrganizationsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('organizations')->delete();
        
        \DB::table('organizations')->insert(array (
            0 => 
            array (
                'id' => 1,
                'created_at' => '2019-04-14 12:19:05',
                'updated_at' => '2019-04-14 12:19:05',
                'name' => 'Organization Aceh',
                'phone' => '089636845137',
                'email' => 'galtekindo@gmail.com',
                'website' => 'www.aceh.com',
                'logo' => '1555219145.png',
                'deleted_at' => NULL,
            ),
            1 => 
            array (
                'id' => 2,
                'created_at' => '2019-04-14 12:19:36',
                'updated_at' => '2019-04-14 12:19:36',
                'name' => 'Organization Makasar',
                'phone' => '0948599545',
                'email' => 'makasar@gmail.com',
                'website' => 'makasar.com',
                'logo' => '1555219176.png',
                'deleted_at' => NULL,
            ),
            2 => 
            array (
                'id' => 3,
                'created_at' => '2019-04-14 12:20:14',
                'updated_at' => '2019-04-14 12:20:14',
                'name' => 'Organization Bali',
                'phone' => '555555555555',
                'email' => 'bali@example.com',
                'website' => 'bali.com',
                'logo' => '1555219214.png',
                'deleted_at' => NULL,
            ),
        ));
        
        
    }
}