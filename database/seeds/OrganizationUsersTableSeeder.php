<?php

use Illuminate\Database\Seeder;

class OrganizationUsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('organization_users')->delete();
        
        \DB::table('organization_users')->insert(array (
            0 => 
            array (
                'id' => 4,
                'user_id' => 2,
                'organization_id' => 1,
                'created_at' => '2019-04-14 12:21:03',
                'updated_at' => '2019-04-14 12:21:03',
            ),
            1 => 
            array (
                'id' => 5,
                'user_id' => 2,
                'organization_id' => 3,
                'created_at' => '2019-04-14 12:21:04',
                'updated_at' => '2019-04-14 12:21:04',
            ),
            2 => 
            array (
                'id' => 6,
                'user_id' => 3,
                'organization_id' => 2,
                'created_at' => '2019-04-14 12:21:13',
                'updated_at' => '2019-04-14 12:21:13',
            ),
            3 => 
            array (
                'id' => 7,
                'user_id' => 4,
                'organization_id' => 1,
                'created_at' => '2019-04-14 12:21:20',
                'updated_at' => '2019-04-14 12:21:20',
            ),
            4 => 
            array (
                'id' => 8,
                'user_id' => 4,
                'organization_id' => 2,
                'created_at' => '2019-04-14 12:21:20',
                'updated_at' => '2019-04-14 12:21:20',
            ),
            5 => 
            array (
                'id' => 9,
                'user_id' => 4,
                'organization_id' => 3,
                'created_at' => '2019-04-14 12:21:20',
                'updated_at' => '2019-04-14 12:21:20',
            ),
        ));
        
        
    }
}