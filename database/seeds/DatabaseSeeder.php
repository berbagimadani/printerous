<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        
       /*\DB::table('users')->delete();
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'email' => 'admin@example.com',
                'password' => Hash::make('admin'),
                'name' => 'Admin',  
                'role_id' => 1,
                'email_verified_at' => NULL,
                'created_at' => '2019-03-05 06:35:23',
                'updated_at' => '2019-03-05 06:35:23', 
            ), 
            array (
                'id' => 2,
                'name' => 'Account Manager 1',
                'email' => 'am1@mail.com',
                'email_verified_at' => NULL,
                'password' => Hash::make('user'),
                'role_id' => 2, 
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            ),
            array (
                'id' => 3,
                'name' => 'Account Manager 2',
                'email' => 'am2@mail.com',
                'email_verified_at' => NULL,
                'password' => Hash::make('user'),
                'role_id' => 2, 
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            ),
             array (
                'id' => 4,
                'name' => 'Account Manager 3',
                'email' => 'am3@mail.com',
                'email_verified_at' => NULL,
                'password' => Hash::make('user'),
                'role_id' => 2, 
                'created_at' => '2019-04-02 23:09:25',
                'updated_at' => '2019-04-02 23:09:25',
            )
        ));
            */
 
        $this->call(UsersTableSeeder::class);
        $this->call(OrganizationsTableSeeder::class);
        $this->call(OrganizationPeoplesTableSeeder::class);
        $this->call(OrganizationUsersTableSeeder::class);
    }
}
