<?php

namespace App;

use Illuminate\Database\Eloquent\Model; 

class Organization extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organizations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'phone', 'email', 'website', 'logo'];

    public static $createRules = [
        'name'  => 'required|max:255',
        'phone' => 'required',
        'email' => 'email|unique:organizations',
        'logo'  => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
    ];
        
    
    public static function updateRules($id) {
        return [
            'name'  => 'required|max:255',
            'phone' => 'required',
            'email' => 'email|unique:organizations,email,'.$id,
            'logo'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }

    public function organization_peoples(){
        return $this->hasMany('App\OrganizationPerson');
    }

    public function organization_users(){
        return $this->hasMany('App\OrganizationUser', 'organization_id');
    }

}
