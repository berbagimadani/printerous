<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrganizationPerson extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'organization_peoples';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'phone', 'email', 'avatar', 'organization_id'];

    public static $createRules = [
        'name'          => 'required|max:255',
        'phone'         => 'required',
        'email'         => 'email|unique:organization_peoples',
        'avatar'        => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'organization_id'   => 'required'
    ];
        
    
    public static function updateRules($id) {
        return [
            'name'  => 'required|max:255',
            'phone' => 'required',
            'email' => 'email|unique:organization_peoples,email,'.$id,
            'avatar'  => 'image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ];
    }
}
