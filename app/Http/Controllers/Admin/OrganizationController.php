<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Organization;
use App\OrganizationPerson;
use Illuminate\Http\Request;

class OrganizationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $organization = Organization::where('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('website', 'LIKE', "%$keyword%")
                ->orWhere('logo', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $organization = Organization::latest()->paginate($perPage);
        }

        return view('admin.organization.index', compact('organization'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('admin.organization.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
    
        $this->validate($request, Organization::$createRules);

        if ($files = $request->logo) { 
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('images/organization'), $imageName); 
            $requestData['logo'] = $imageName;
        }

        Organization::create($requestData);

        return redirect('admin/organization')->with('flash_message', 'Organization added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show(Request $request, $id)
    {
        $organization = Organization::findOrFail($id);
        $keyword = $request->get('search');
        $perPage = 25;

        //$organization_peoples = $organization->organization_peoples()->get();
         if (!empty($keyword)) {
           $organization_peoples = OrganizationPerson::whereOrganizationId($id)
                ->where('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%") 
                ->latest()
                ->paginate($perPage);
        } else {
            $organization_peoples = $organization->organization_peoples()->get();
        }

 
        return view('admin.organization.show', compact('organization', 'organization_peoples'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $organization = Organization::findOrFail($id);

        return view('admin.organization.edit', compact('organization'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $this->validate($request, Organization::updateRules($id));

        $organization = Organization::findOrFail($id);

        if ($files = $request->logo) { 
            $imageName = time().'.'.request()->logo->getClientOriginalExtension();
            request()->logo->move(public_path('images/organization'), $imageName); 
            $requestData['logo'] = $imageName;
        }

        $organization->update($requestData);

        return redirect('admin/organization')->with('flash_message', 'Organization updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        Organization::destroy($id);

        return redirect('admin/organization')->with('flash_message', 'Organization deleted!');
    }
}
