<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Organization;
use App\OrganizationUser;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $users = User::latest()->paginate($perPage);
        } else {
            $users = User::select( 
                    'users.id',
                    'users.name',
                    'users.email',
                    'users.role_id',  
                    'users.created_at'
                ) 
                ->latest()
                ->paginate($perPage);
        }

        return view('admin.users.index', compact('users'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {   
        $organizations = Organization::get();
        return view('admin.users.create', compact('organizations'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        User::create($requestData);

        return redirect('admin/users')->with('flash_message', 'User added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('admin.users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        $organizations = Organization::get();
        return view('admin.users.edit', compact('user', 'organizations'));
    }

    /*
    * attachOrganization
    */
    public function attachOrganization($id) {

        $user = User::findOrFail($id); 

        $ownOrganizations = Organization::whereHas('organization_users', function($q) use($id) {
            $q->where('organization_users.user_id', $id);
        })->pluck('id', 'name')->toArray();

        $notOrganization = Organization::whereNotIn('id', $ownOrganizations)->pluck('id', 'name')->toArray();
 
        return view('admin.users.attach', compact('user', 'notOrganization', 'ownOrganizations'));
    }

    public function postOrganization(Request $request){
        $requestData = $request->all();

        if(!empty($request->organization)) {
        organizationUser::whereUserId($request->user_id)->delete();
        foreach ($request->organization as $key => $value) {
            $user = organizationUser::whereUserId($request->user_id)->whereOrganizationId($value)->first();
            if(is_null($user)){
                $organization = new organizationUser();
                $organization->user_id = $request->user_id;
                $organization->organization_id = $value;
                $organization->save();
            }
        } 
        }
        return redirect('admin/users')->with('flash_message', 'User updated!');
    }
    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $user = User::findOrFail($id);
        $user->update($requestData);

        return redirect('admin/users')->with('flash_message', 'User updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        User::destroy($id);

        return redirect('admin/users')->with('flash_message', 'User deleted!');
    }
}
