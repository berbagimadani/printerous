<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\OrganizationPerson;
use Illuminate\Http\Request;

class OrganizationPeopleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index(Request $request)
    {
        $keyword = $request->get('search');
        $perPage = 25;

        if (!empty($keyword)) {
            $organizationpeople = OrganizationPerson::where('name', 'LIKE', "%$keyword%")
                ->orWhere('phone', 'LIKE', "%$keyword%")
                ->orWhere('email', 'LIKE', "%$keyword%")
                ->orWhere('avatar', 'LIKE', "%$keyword%")
                ->orWhere('organization_id', 'LIKE', "%$keyword%")
                ->latest()->paginate($perPage);
        } else {
            $organizationpeople = OrganizationPerson::latest()->paginate($perPage);
        }

        return view('admin.organization-people.index', compact('organizationpeople'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create($id)
    {
        return view('admin.organization-people.create')->with('id', $id);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(Request $request)
    {
        
        $requestData = $request->all();
        
        $this->validate($request, OrganizationPerson::$createRules);

        if ($files = $request->avatar) { 
            $imageName = time().'.'.request()->avatar->getClientOriginalExtension();
            request()->avatar->move(public_path('images/avatar'), $imageName); 
            $requestData['avatar'] = $imageName;
        }

        OrganizationPerson::create($requestData);   

        return redirect('admin/organization/'.$request->organization_id)->with('flash_message', 'OrganizationPerson added!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $organizationperson = OrganizationPerson::findOrFail($id);

        return view('admin.organization-people.show', compact('organizationperson'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $organizationperson = OrganizationPerson::findOrFail($id);

        return view('admin.organization-people.edit', compact('organizationperson'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(Request $request, $id)
    {
        
        $requestData = $request->all();
        
        $this->validate($request, OrganizationPerson::updateRules($id));

        $organizationperson = OrganizationPerson::findOrFail($id);
        
        if ($files = $request->avatar) { 
            $imageName = time().'.'.request()->avatar->getClientOriginalExtension();
            request()->avatar->move(public_path('images/avatar'), $imageName); 
            $requestData['avatar'] = $imageName;
        }

        $organizationperson->update($requestData);

        return redirect('admin/organization/'.$organizationperson->organization_id)->with('flash_message', 'OrganizationPerson updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     *
     * @return \Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function destroy($id, Request $request)
    {
        $org = OrganizationPerson::destroy($id);

        return redirect('admin/organization/'.$request->organization_id)->with('flash_message', 'OrganizationPerson deleted!');
    }
}
