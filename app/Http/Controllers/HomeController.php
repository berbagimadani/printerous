<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Organizationuser;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {   
        $organizations = OrganizationUser::select(  
                'organizations.name as organization_name'
            )
            ->leftJoin('organizations', 'organization_users.organization_id', '=', 'organizations.id')
            ->where('user_id',Auth::user()->id)
            ->get();

        return view('home',compact('organizations'));
    }
}
