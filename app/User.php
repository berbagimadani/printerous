<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use App\OrganizationUser;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'organization_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function ownOrganization($organization){
        if($organization== $this->attachOrganizationPerson($organization)){
            return true;
        } elseif($this->isAdmin()) {
            return true;
        }
    }
    
    /*
    * Hard code previllage users
    * role_id:
    * admin = 1
    * account manager = 2
    */

    public function isAdmin(){
        if($this->role_id==1){
            return true;
        } 
    }
    public function isAccountManager(){
        if($this->role_id==2){
            return true;
        } 
    }

    public function attachOrganizationPerson($organization){
        $organization = OrganizationUser::whereUserId($this->id)->whereOrganizationId($organization)->first();
        return $organization['organization_id'];
    }

}
