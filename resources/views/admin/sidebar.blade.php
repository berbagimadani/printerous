<div class="col-md-3">
    <div class="card">
        <div class="card-header">
            Sidebar
        </div>

        <div class="card-body">
            <ul class="nav" role="tablist">
                <li role="presentation">
                    <a href="{{ url('/home') }}">
                        Dashboard
                    </a>
                </li>
                
                <!-- account manager -->
                @if(Auth::user()->isAccountManager()) 
                <li>
                    <a href="{{ url('/admin/organization') }}">
                        Organization
                    </a>
                </li>
                @endif

                <!--  Admin  -->
                @if(Auth::user()->isAdmin()) 
                <hr> 
                <li>
                    <a href="{{ url('/admin/organization') }}">
                        Organization
                    </a>
                </li>
                <li>
                    <a href="{{ url('/admin/users') }}">
                        Users
                    </a>
                </li> 
                <!--<li>
                    <a href="{{ url('/admin/organization-people') }}">
                        Detail Organization
                    </a>
                </li> -->
                @endif


            </ul> 
        </div>
    </div>
</div>
