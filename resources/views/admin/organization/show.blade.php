@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Organization "{{ $organization->name }}"</div>
                    <div class="card-body">
                        
                        <a href="{{ url('/admin/organization') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>

                        @if(Auth::user()->role_id==1)
                        <!--
                        <a href="{{ url('/admin/organization/' . $organization->id . '/edit') }}" title="Edit Organization"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/organization' . '/' . $organization->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete Organization" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        -->
                         @endif
                    
                        <br/>
                        <br/>
                       

                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th colspan="4">
                                            @if(file_exists( public_path('/images/organization/').$organization->logo))
                                            <img src="{{ url('/images/organization/', array($organization->logo))}}" width="200">
                                            @endif
                                        </th>
                                    </tr>
                                </thead>
                                <tbody> 
                                    <tr><th> Name </th><td> {{ $organization->name }} </td></tr>
                                    <tr><th> Phone </th><td> {{ $organization->phone }} </td></tr>
                                    <tr><th> Website </th><td> {{ $organization->website }} </td></tr>
                                </tbody>
                            </table>
                        </div>
 
                    </div>
                </div>

                <div class="card">
                        
                    <div class="card-header">
                        <form method="GET" action="{{ url('/admin/organization', array($organization->id)) }}" accept-charset="UTF-8" class="form-inline my-2 my-lg-0 float-right" role="search">
                            <div class="input-group">
                                <input type="text" class="form-control" name="search" placeholder="Search..." value="{{ request('search') }}">
                                <span class="input-group-append">
                                    <button class="btn btn-secondary" type="submit">
                                        <i class="fa fa-search"></i>
                                    </button>
                                </span>
                            </div>
                        </form>
                    </div>
                    <div class="card-header">List Persons ( PIC )
                        <!-- Account Manager dari Organization tersebut -->  
                        @if(Auth::user()->ownOrganization($organization->id))
                            <a href="{{ url('/admin/organization-people/create', array($organization->id)) }}" style="float:right"><button class="btn btn-primary btn-sm"><i class="fa fa-plus" aria-hidden="true"></i> +Add PIC</button></a>
                        @endif
                     </div>
                     <div class="card-body">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <td>Avatar</td>
                                        <td>Name</td>
                                        <td>Email</td>
                                        <td>Phone</td>
                                        @if(Auth::user()->ownOrganization($organization->id))
                                        <td>Action</td>
                                        @endif
                                    </tr>
                                </thead>
                                <tbody> 
                                    @foreach($organization_peoples as $person)
                                    <tr>
                                        <td>
                                            @if(file_exists( public_path('/images/avatar/').$person->avatar) && !empty($person->avatar))
                                            <img src="{{ url('/images/avatar/', array($person->avatar))}}" width="100">
                                            @endif
                                        </td>
                                        <td>{{ $person->name }}</td>
                                        <td>{{ $person->email }}</td>
                                        <td>{{ $person->phone }}</td>
                                        <td> 
                                        @if(Auth::user()->ownOrganization($organization->id))
                                            <a href="{{ url('/admin/organization-people/' . $person->id . '/edit') }}" title="Edit OrganizationPerson"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                                            <form method="POST" action="{{ url('/admin/organization-people' . '/' . $person->id) }}" accept-charset="UTF-8" style="display:inline">
                                                {{ method_field('DELETE') }}
                                                {{ csrf_field() }}
                                                <input type="hidden" name="organization_id" value="{{$organization->id}}">

                                                <button type="submit" class="btn btn-danger btn-sm" title="Delete OrganizationPerson" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                                            </form>
                                        @endif
                                        </td>
                                    </tr> 
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>


            </div>
        </div>
    </div>
@endsection
