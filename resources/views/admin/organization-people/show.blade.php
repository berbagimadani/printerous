@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">OrganizationPerson {{ $organizationperson->id }}</div>
                    <div class="card-body">

                        <a href="{{ url('/admin/organization-people') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <a href="{{ url('/admin/organization-people/' . $organizationperson->id . '/edit') }}" title="Edit OrganizationPerson"><button class="btn btn-primary btn-sm"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Edit</button></a>

                        <form method="POST" action="{{ url('admin/organizationpeople' . '/' . $organizationperson->id) }}" accept-charset="UTF-8" style="display:inline">
                            {{ method_field('DELETE') }}
                            {{ csrf_field() }}
                            <button type="submit" class="btn btn-danger btn-sm" title="Delete OrganizationPerson" onclick="return confirm(&quot;Confirm delete?&quot;)"><i class="fa fa-trash-o" aria-hidden="true"></i> Delete</button>
                        </form>
                        <br/>
                        <br/>

                        <div class="table-responsive">
                            <table class="table">
                                <tbody>
                                    <tr>
                                        <th>ID</th><td>{{ $organizationperson->id }}</td>
                                    </tr>
                                    <tr><th> Name </th><td> {{ $organizationperson->name }} </td></tr><tr><th> Phone </th><td> {{ $organizationperson->phone }} </td></tr><tr><th> Email </th><td> {{ $organizationperson->email }} </td></tr>
                                </tbody>
                            </table>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
