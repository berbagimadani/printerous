@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            @include('admin.sidebar')

            <div class="col-md-9">
                <div class="card">
                    <div class="card-header">Attach Organization "{{ $user->name }}"</div>
                    <div class="card-body">
                        <a href="{{ url('/admin/users') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back</button></a>
                        <br />
                        <br />

                        @if ($errors->any())
                            <ul class="alert alert-danger">
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        @endif

                        <form method="POST" action="{{ url('/admin/postOrganization/') }}" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
                             
                            {{ csrf_field() }}

                            @if($user->role_id==2)

                            <input type="hidden" value="{{ $user->id }}" name="user_id">
                            @foreach($ownOrganizations as $key => $row) 
                            <div class="form-group"> 
                                <label>
                                    <input type="checkbox" value="{{$row}}" name="organization[]" checked=""> {{$key}}
                                </label>
                            </div>
                            @endforeach 

                            @foreach($notOrganization as $key => $row) 
                            <div class="form-group"> 
                                <label>
                                    <input type="checkbox" value="{{$row}}" name="organization[]"> {{$key}}
                                </label>
                            </div>
                            @endforeach 

                            @endif

                            <div class="form-group">
                                <input class="btn btn-primary" type="submit" value="Atach">
                            </div>


                        </form>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
